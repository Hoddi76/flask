from flask import Blueprint, render_template, flash, redirect, jsonify, json, url_for, session
from forms import FullForm, P_Form, Yy_form, DemoForm
from flask import request

from stoimost import order_stoimost
from decimal import Decimal

from models import Order
from app import db, q
from emails import send_new_email
from full_parser import main
from demo_parser import demo_main

import hashlib

parsers = Blueprint('parser', __name__, template_folder='templates', static_folder='static')


@parsers.route('', methods=['GET', 'POST'])
def parser():
    form = FullForm(request.form)
    p_form = P_Form()
    if form.validate():
        if request.method == 'POST' and request.form['pay_btn'] == 'Расчитать стоимость':
            ref = request.form['url_avito']
            limit = request.form['limit']

            ### Чек боксы ###
            contact_face = form.contact_face.data  # Контактное лицо
            ad_title = form.ad_title.data  # Заголовок объявления
            ad_description = form.ad_description.data  # Описание объявления
            ad_price = form.ad_price.data  # Цена объявления
            city = form.city.data  # Город
            address = form.address.data  # Район, метро, адрес
            ad_link = form.ad_link.data  # Ссылка на объявление

            session['checkbox'] = {
                'contact_face': contact_face,
                'ad_title': ad_title,
                'ad_description': ad_description,
                'ad_price': ad_price,
                'city': city,
                'address': address,
                'ad_link': ad_link
            }

            result = (order_stoimost(ref))
            p = Decimal(result[0])
            price = p.quantize(Decimal("1.00"))
            kol_vo_ob = result[-1]

            flash('{}'.format(price, kol_vo_ob))

            return render_template('parser/parser_full.html', form=form, price=price, kol_ob=kol_vo_ob, p_form=p_form)

        elif request.method == 'POST' and request.form['pay_btn'] == 'Оплатить заказ':
            ref = request.form['url_avito']
            limit = request.form['limit']

            result = (order_stoimost(ref))
            p = Decimal(result[0])
            price = p.quantize(Decimal("1.00"))
            kol_vo_ob = result[-1]
            email = request.form['email']
            pay_select = request.form['pay_select']
            type_file = request.form['type_file']

            # Прайс с комиссией
            commision_price = None
            if pay_select == '1':
                """ Банковская карта """
                commision_price_dem = price / 100 * 2 + price
                pr = Decimal(commision_price_dem)
                commision_price = pr.quantize(Decimal("1.00"))  # Цена с учетом комисии
                pay = 'Банковская карта (комиссия 2%)'
                pay_type = 'AC'
            elif pay_select == '2':
                """ Яндекс деньги """
                commision_price_dem = float(price) / 100 * 0.5 + float(price)
                pr = Decimal(commision_price_dem)
                commision_price = pr.quantize(Decimal("1.00"))
                pay = 'Яндекс.Деньги (комиссия 0,5%)'
                pay_type = 'PC'

            session['data'] = {'k': kol_vo_ob,  # Количество объявлений
                               'price': float(price),  # Стоимость без комисии
                               'commision_price': float(commision_price),  # Стоимость с комисией
                               'pay': pay,  # Способ оплаты
                               'email': email,
                               'ref': ref,
                               'pay_type': pay_type,
                               'type_file': type_file,}

            # Заносим заказ в бд
            try:
                order = Order(ref=ref,
                              email=email,
                              price=str(commision_price),
                              ad_title=form.contact_face.data,
                              ad_price=form.ad_price.data,
                              contact_face=form.contact_face.data,
                              ad_description=form.ad_description.data,
                              city=form.city.data,
                              address=form.address.data,
                              ad_link=form.ad_link.data,
                              type_file=type_file,
                              )
                db.session.add(order)
                db.session.commit()
            except:
                None
                # print('sdf')

            # print(float(price))
            # return render_template('parser/parser_full.html', form=form, price=price, kol_ob=kol_vo_ob)
            # return json.dump({'price': int(price)})
            # return render_template('parser/modal.html', k=kol_vo_ob, price=price, commision_price=commision_price, pay=pay,
            # email=email)
            return redirect(url_for('parser.pay'))


    else:
        return render_template('parser/parser_full.html', form=form)


# return 'OK'


@parsers.route('/pay', methods=['GET', 'POST'])
def pay():
    data = session.get('data')
    checkbox = session.get('checkbox')
    form = Yy_form

    # orders = Order.query.all()[-1]
    ordd = Order.query.filter(Order.email == data['email'])[-1]
    # print(order.id)
    # orders = Order.query.filter(Order.ref == data['ref']).first()
    # send_new_email(orders)  # Выполняет отправку почты
    # return 'OKs'
    if request.method == 'GET' and ordd.pay_status == True:
        return redirect('http://avito-parser24.ru/orders/' + ordd.slug)
    return render_template('parser/modal.html', order=ordd, form=form, k=data['k'], price=data['price'],
                           commision_price=data['commision_price'],
                           pay=data['pay'], email=data['email'], pay_type=data['pay_type'])


@parsers.route('/pay_verification', methods=['POST'])
def pay_verification():
    if request.method == 'POST':
        # z = request.data['label']
        # print(z)
        # print('Yvedomlenie prishlo')
        s = request.get_data()
        # print(s)
        lab = request.form['sha1_hash']
        # print(lab)
        notification_type = request.form['notification_type']
        operation_id = request.form['operation_id']
        amount = request.form['amount']
        currency = request.form['currency']
        datetime = request.form['datetime']
        sender = request.form['sender']
        codepro = request.form['codepro']
        notification_secret = 'azJ7LW0O0Bw4zS2L7RDTtXAl'
        label = request.form['label']
        sha1_hash = request.form['sha1_hash']

        sha1_str = notification_type + '&' + operation_id + '&' + amount + '&' + currency + '&' + datetime + '&' + sender + '&' + codepro + '&' + notification_secret + '&' + label
        sha1 = hashlib.sha1(sha1_str.encode('utf-8')).hexdigest()

        if sha1_hash == sha1:
            order = Order.query.filter(Order.id == int(label)).first()
            # print(order.id)
            # order = Order.query.get(int(label))
            # Запускаем задачу в редис
            j = q.enqueue(main, order.ref, order.ad_title, order.ad_price, order.contact_face, order.ad_description,
                          order.city,
                          order.address, order.ad_link, order.type_file)
            id_worker = j.get_id()  # Узнаем id процесса
            # order_new = order(pay_status=True, id_worker=id_worker)
            order.pay_status = True
            order.id_worker = id_worker
            db.session.add(order)
            db.session.commit()

            send_new_email(order)  # Выполняет отправку почты
            return 'HTTP 200 OK'
    return 'HTTP 200 OK'
    #  Тут начинаем передачу задачи в редис


# ha = hashlib.sha1(b'Hello Python').hexdigest()

@parsers.route("/demo_parser", methods=['GET', 'POST'])
def demo_parser():
    form = DemoForm(request.form)
    if request.method == 'POST' and form.validate():
        ref = request.form['url_avito']
        email = request.form['email']

        ### Чек боксы ###
        contact_face = form.contact_face.data  # Контактное лицо
        ad_title = form.ad_title.data  # Заголовок объявления
        ad_description = form.ad_description.data  # Описание объявления
        ad_price = form.ad_price.data  # Цена объявления
        city = form.city.data  # Город
        address = form.address.data  # Район, метро, адрес
        ad_link = form.ad_link.data  # Ссылка на объявление

        type_file = request.form['type_file']

        # Запускаем процес в редис
        j = q.enqueue(demo_main,
                      ref,
                      ad_title,
                      ad_price,
                      contact_face,
                      ad_description,
                      city,
                      address,
                      ad_link,
                      type_file,
                      )

        id_worker = j.get_id()  # Узнаем id процесса
        try:
            order = Order(ref=ref, email=email, id_worker=id_worker, type_file=type_file)
            db.session.add(order)
            db.session.commit()
        except:
            None

        orders = Order.query.all()[-1]

        send_new_email(orders)  # Выполняет отправку почты

        flash('{}'.format(orders.id))  # Полезный способ показать сообщение пользователю.
        return render_template('parser/demo_parser.html', form=form, orders=orders)
    return render_template('parser/demo_parser.html', form=form)
