from flask_mail import Message
from app import mail, app

from flask import render_template, current_app
from threading import Thread

#app = create_app()


def send_async_email(app,msg):
    # Создаёт фоновый поток
    with app.app_context():
        mail.send(msg)


def send_email(subject, sender, recipients, text_body, html_body):
    """
Отправляет письмо с адреса указаного в config
    :param subject: Заголовок письма
    :param sender: Отправитель
    :param recipients: Список получателей
    :param text_body: Текстовый вид
    :param html_body: Html вид
    """
    msg = Message(subject, sender=sender, recipients=recipients)
    msg.body = text_body
    msg.html = html_body
    Thread(target=send_async_email, args=(app, msg)).start()


def send_new_email(orders):
    send_email('Заказ № ' + str(orders.id) + ' на Avito-Parser24.ru',  # Незабыть исправить заголовок
               sender='sapport@avito-parser24.ru',
               recipients=[orders.email],
               text_body=render_template('emails.txt',
                                         orders=orders),
               html_body=render_template('emails.html',
                                         orders=orders))


#def send_feedback():
