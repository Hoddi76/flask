"""empty message

Revision ID: 72832e90f095
Revises: 
Create Date: 2018-07-13 14:47:11.940320

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '72832e90f095'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('order',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('slug', sa.String(length=140), nullable=True),
    sa.Column('time', sa.DateTime(), nullable=True),
    sa.Column('ref', sa.String(length=200), nullable=True),
    sa.Column('number_ads', sa.Integer(), nullable=True),
    sa.Column('save_ads', sa.Integer(), nullable=True),
    sa.Column('email', sa.String(length=120), nullable=True),
    sa.Column('complete', sa.Boolean(), nullable=True),
    sa.Column('id_worker', sa.String(length=36), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('slug')
    )
    op.create_table('role',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=100), nullable=True),
    sa.Column('description', sa.String(length=255), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('name')
    )
    op.create_table('user',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('email', sa.String(length=100), nullable=True),
    sa.Column('password', sa.String(length=255), nullable=True),
    sa.Column('active', sa.Boolean(), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('email')
    )
    op.create_table('role_users',
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.Column('role_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['role_id'], ['role.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], )
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('role_users')
    op.drop_table('user')
    op.drop_table('role')
    op.drop_table('order')
    # ### end Alembic commands ###
