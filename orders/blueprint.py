from flask import Blueprint, render_template, send_from_directory, send_file, request, jsonify, json
from models import Order

orders = Blueprint('orders', __name__, template_folder='templates', static_folder='static')


@orders.route('/<slug>', methods=['GET', 'POST'])
# Ссылка на конкретный заказ
def order_detail(slug):
    order = Order.query.filter(Order.slug == slug).first_or_404()
    name_zakaz = order.id
    name_file = 'Заказ №' + str(name_zakaz) + '.' + order.type_file
    #time = order.time.split('.')[0]
    # file = send_from_directory('/home/artem/Рабочий стол/parser_site/parser_site/' + name_file, name_file)
    if request.method == 'POST':
        try:
            save_ads = order.get_save_ads()  # Скачаные объявления
            collected_ads = order.get_collected_ob()
            progress = str(order.get_progress()) + '%'
            bar = 'Парсинг объявлений готовность ' + str(int(order.get_progress())) + '%'
            prog = order.get_progress()
        except:
            save_ads = order.save_ads
            collected_ads = order.number_ads
            progress = '100%'
            bar = 'Парсинг объявлений готовность ' + str(int(order.get_progress())) + '%'
            prog = 100


        order_complite = str(order.complete)

        return json.dumps({'save_ads': save_ads,
                           'collected_ads': collected_ads,
                           'progress': progress,
                           'bar': bar,
                           'prog': prog,
                           'complite': order_complite})

        # return render_template('orders/ads.html', ads=str(ads))

    return render_template('orders/index.html', orders=order, name_file=name_file, time=order.time)


@orders.route('/download/<filename>')
# Ссылка на скачку файла
def download(filename):
    return send_from_directory('/root/www/flask/zakaz', filename)
