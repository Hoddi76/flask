import os


class Configuration(object):
    DEBUG = True
    ALLOWED_HOSTS = ['avito-parser24.ru', '127.0.0.1', 'localhost']
    MAIL_SERVER = 'smtp.yandex.ru'
    MAIL_PORT = 465
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True
    MAIL_USERNAME = 'sapport@avito-parser24.ru'
    MAIL_PASSWORD = 'pro1lolo'

    # administrator list
    ADMINS = ['sapport@avito-parser24.ru']

    REDIS_URL = os.environ.get('REDIS_URL') or 'redis://'

    SECRET_KEY = os.environ.get(
        'SECRET_KEY') or 'm\xbf\xc4\xd0e\xed\xd5\x1b\x88\xe1\xfe\xe1\xb9X\r\\\xfaO\xa7\xdem\xdb\xa6\xba'

    basedir = os.path.abspath(os.path.dirname(__file__))

    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
                              'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # Flask-Security config
    SECURITY_URL_PREFIX = "/admin"
    SECURITY_PASSWORD_HASH = "pbkdf2_sha512"
    SECURITY_PASSWORD_SALT = "ATGUOHAELKiubahiughaerGOJAEGj"

    # Flask-Security URLs, overridden because they don't put a / at the end
    SECURITY_LOGIN_URL = "/login/"
    SECURITY_LOGOUT_URL = "/logout/"
    SECURITY_REGISTER_URL = "/register/"

    SECURITY_POST_LOGIN_VIEW = "/admin/"
    SECURITY_POST_LOGOUT_VIEW = "/admin/"
    SECURITY_POST_REGISTER_VIEW = "/admin/"

    # Flask-Security features
    SECURITY_REGISTERABLE = True
    SECURITY_SEND_REGISTER_EMAIL = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
