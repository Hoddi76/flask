from app import db
from datetime import datetime
import random
import re
import redis
import rq

from flask_security import UserMixin, RoleMixin


def new_url():
    # Генерируем дополнение к слагу
    hash_url = random.sample('1234567890qwertyuiopasdfghjklzxcvbnm', 25)
    new = ''.join(hash_url)
    return new


class Order(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    slug = db.Column(db.String(140), unique=True)  # ЧПУ - человеко подобный урл (урл отслеживания заказа)
    time = db.Column(db.DateTime, default=datetime.now())
    ref = db.Column(db.String(200))  # Ссылка с фильтрами авито(предоставляемая через форму)
    number_ads = db.Column(db.Integer)  # Кол-во объявлений в заказе
    save_ads = db.Column(db.Integer)  # Кол-во уже скачанных объявлений
    email = db.Column(db.String(120))
    complete = db.Column(db.Boolean, default=False)  # Статус выполнения заказа
    id_worker = db.Column(db.String(36))  # id задачи
    price = db.Column(db.String(30))  # Цена (с учетом комиссии)
    pay_status = db.Column(db.Boolean, default=False)  # Статус платежа
    # Значения чек боксов
    ad_title = db.Column(db.Boolean, default=False)
    ad_price = db.Column(db.Boolean, default=False)
    contact_face = db.Column(db.Boolean, default=False)
    ad_description = db.Column(db.Boolean, default=False)
    city = db.Column(db.Boolean, default=False)
    address = db.Column(db.Boolean, default=False)
    ad_link = db.Column(db.Boolean, default=False)

    type_file = db.Column(db.String(30))

    def __init__(self, *args, **kwargs):
        super(Order, self).__init__(*args, **kwargs)
        self.generate_slug()

    def generate_slug(self):
        # Генирируем слаг
        self.slug = new_url()

    def get_rq_job(self):
        try:
            rq_job = rq.job.Job.fetch(self.id_worker, connection=redis.Redis())
        except (redis.exceptions.RedisError, rq.exceptions.NoSuchJobError):
            return None

        return rq_job

    def get_progress(self):
        # Выводит прогресс(meta[progress])
        job = self.get_rq_job()
        return job.meta.get('progress', 0) if job is not None else 100

    def get_collected_ob(self):
        # Выводит кол-во собранных объявлений
        job = self.get_rq_job()
        return job.meta.get('collected_ob')

    def get_save_ads(self):
        job = self.get_rq_job()
        one_save_ads = Order.query.filter(Order.save_ads).first()
        return job.meta.get('save_ads', '') if job is not None else one_save_ads


### Flask Security

# Доп таблица для классов юзер и роль тк отношение мэни ту мэни
role_users = db.Table('role_users',
                      db.Column('user_id', db.Integer, db.ForeignKey('user.id')),
                      db.Column('role_id', db.Integer, db.ForeignKey('role.id'))
                      )


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    roles = db.relationship('Role', secondary=role_users, backref=db.backref('users',
                                                                             lazy='dynamic'))  # Выстраиваем отнашения между Юзером и Ролью через таблицу Роли_Юзера


class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=True)  # Имя роли (например Админ)
    description = db.Column(db.String(255))  # Описание роли

### Blog ###


def slugify(name):
    """
    Генерируем слаг
    Slug generate и
    транслитим его
    :param name = строке которую надо транслитить
    """
    # Слоаврь с заменами
    slovar = {'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'e',
              'ж': 'zh', 'з': 'z', 'и': 'i', 'й': 'i', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n',
              'о': 'o', 'п': 'p', 'р': 'r', 'с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'h',
              'ц': 'c', 'ч': 'ch', 'ш': 'sh', 'щ': 'scz', 'ъ': '', 'ы': 'y', 'ь': '', 'э': 'e',
              'ю': 'u', 'я': 'ya', 'А': 'a', 'Б': 'b', 'В': 'v', 'Г': 'g', 'Д': 'd', 'Е': 'e', 'Ё': 'e',
              'Ж': 'zh', 'З': 'z', 'И': 'i', 'Й': 'i', 'К': 'k', 'Л': 'l', 'М': 'm', 'Н': 'n',
              'О': 'o', 'П': 'p', 'Р': 'r', 'С': 's', 'Т': 't', 'У': 'u', 'Ф': 'f', 'Х': 'h',
              'Ц': 'c', 'Ч': 'ch', 'Ш': 'sh', 'Щ': 'scz', 'Ъ': '', 'Ы': 'y', 'Ь': '', 'Э': 'e',
              'Ю': 'u', 'Я': 'ya', ',': '', '?': '', ' ': '-', '~': '', '!': '', '@': '', '#': '',
              '$': '', '%': '', '^': '', '&': '', '*': '', '(': '', ')': '', '-': '-', '=': '', '+': '',
              ':': '', ';': '', '<': '', '>': '', '\'': '', '"': '', '\\': '', '/': '', '№': '',
              '[': '', ']': '', '{': '', '}': '', 'ґ': '', 'ї': '', 'є': '', 'Ґ': 'g', 'Ї': 'i',
              'Є': 'e'}

    # Циклически заменяем все буквы в строке
    for key in slovar:
        name = name.replace(key, slovar[key])
    return name.lower()


post_categories = db.Table('post_categories',
                           db.Column('post_id', db.Integer, db.ForeignKey('post.id')),
                           db.Column('categories_id', db.Integer, db.ForeignKey('categories.id')),
                           )


class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(140))
    slug = db.Column(db.String(140), unique=True)
    link_image = db.Column(db.String(200))
    body = db.Column(db.Text)
    categories = db.relationship('Categories',
                                 secondary=post_categories,
                                 backref=db.backref('posts', lazy='dynamic'))

    def __init__(self, *args, **kwargs):
        super(Post, self).__init__(*args, **kwargs)
        self.generate_slug()

    def generate_slug(self):
        if self.title:
            self.slug = slugify(self.title)

    def __repr__(self):
        return '<Post id: {}, title: {}>'.format(self.id, self.title)


class Categories(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    slug = db.Column(db.String(100))

    def __init__(self, *args, **kwargs):
        super(Categories, self).__init__(*args, **kwargs)
        self.slug = slugify(self.name)

    def __repr__(self):
        return '<Tag id: {}, name: {}>'.format(self.id, self.name)


# Задавать время создания поста, луче так:
# from sqlalchemy.sql.functions import current_timestamp
# created = db.Column(db.DateTime, default=current_timestamp())
# тогда не будет бага с сортировкой.﻿