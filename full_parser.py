from bs4 import BeautifulSoup
import requests
import time
import random
import csv
import re

from rq import get_current_job

from app import app, db, create_app
from config import Configuration

from models import Order

from emails_result import send_new_emails123
from emails import send_new_email

from openpyxl import Workbook
from openpyxl.styles import PatternFill, Border, Side, Alignment, Protection, Font, Side

app = create_app()
app.config.from_object(Configuration)
db = db

"""
1) Загружаем ссылку
2) Провеояем на кол-во объявлений
3) Парсим
"""


def data_creation():
    pass


def url_generate(url):
    """
    Приводим url к нормальному значеню,
    десктопная версия + вид списка list
    """
    desktop_url = re.sub(r'm\.', 'www.', url)
    if 'view=' in desktop_url:
        new_url = re.sub(r'view=gallery', 'view=list', desktop_url)
    else:
        if '?' in desktop_url:
            new_url = desktop_url + '&view=list'
        else:
            new_url = desktop_url + '?view=list'
    return new_url


def get_html(url, useragent=None, proxy=None):
    r = requests.get(url, headers=useragent, proxies=proxy)
    return r.text


def count_ads(html):
    """
    Подсчитывает количесво
    объявлений на странице
    :return Число объяв.
    """
    soup = BeautifulSoup(html, 'lxml')
    count_ads = soup.find('div', class_='breadcrumbs').find('span', class_='breadcrumbs-link-count').text.strip()
    s = count_ads.replace(' ', '')

    return int(s)


def pagination_on(html):
    """
    Проверяем есть ли пагинация

    """
    soup = BeautifulSoup(html, "lxml")
    try:
        pages = soup.find('div', class_='pagination-pages').findAll('a', class_='pagination-page')[-1].get('href')
        pagination = True
    except:
        pagination = False

    return pagination


def get_total_pages(html):
    # Ищем кол-во страниц
    soup = BeautifulSoup(html, "lxml")
    try:
        pages = soup.find('div', class_='pagination-pages').findAll('a', class_='pagination-page')[-1].get('href')
        total_pages = pages.split('=')[1].split('&')[0]
    except:
        total_pages = 1

    return int(total_pages)


def find_sections(html):
    """
    Ищем ссылки на разделы

    """
    section = []
    soup = BeautifulSoup(html, 'lxml')

    sections = soup.find('div', class_='catalog-counts').findAll('a', class_='js-catalog-counts__link')
    for x in sections:
        section.append('https://www.avito.ru' + x.get('href'))
    # print(section)
    return section


def get_page_data(html):
    """
    Собираем список всех
    объявлений на странице

    """
    soup = BeautifulSoup(html, "lxml")
    try:
        ads = soup.find('div', class_='catalog-list').findAll('div', class_='item')
    except:
        ads = soup.find('div', class_='js-catalog_before-ads').findAll('div', class_='item')

    return ads


def data_generate(ad_title, ad_price, contact_face, ad_description, city, address, ad_link):
    data = [['Телефон']]
    if ad_title == True:
        data[0].append('Заголовок')
    if ad_price == True:
        data[0].append('Цена')
    if contact_face == True:
        data[0].append('Контактное лицо')
    if ad_description == True:
        data[0].append('Описание')
    if city == True:
        data[0].append('Город')
    if address == True:
        data[0].append('Адрес')
    if ad_link == True:
        data[0].append('Ссылка')

    return data


def item_info(soup, ad_title, ad_price, contact_face, ad_description, city, address, ad_link):
    """
    Если чек бокс равен Tru,
    Парсим нужную информацию
    из объявления

    """
    local_data = []  # Итоговый список дата этой функции
    # Номер телефона
    try:
        try:
            url_ajax = soup.find('a', attrs={'data-marker': "item-contact-bar/call"}).get('href')
            number = str(url_ajax).split(':')[-1]
        except:
            number = ''
    except:
        try:
            url_ajax = soup.find('div', class_='_1gYIv').find('a', class_='_1spV2').get('href')
            number = str(url_ajax).split(':')[-1]
        except:
            number = ''
    local_data.append(number)

    # Название объявления
    if ad_title == True:
        try:
            nazvanie_ob = soup.find('span', attrs={'data-marker': "item-description/title"}).text.strip()
        except:
            nazvanie_ob = ''
        local_data.append(nazvanie_ob)

    # Цена
    if ad_price == True:
        try:
            price = soup.find('div', attrs={'data-marker': "item-description/price"}).text.strip()
        except:
            price = ''
        local_data.append(price)

    # Контактное лицо
    if contact_face == True:
        try:
            name = soup.find('span', attrs={'data-marker': "seller-info/name"}).text.strip()
        except:
            # compyani_name = ''
            name = ''
        local_data.append(name)

    # Описание объявления
    if ad_description == True:
        try:
            opisanie = soup.find('div', class_='_1rqyL').find('meta', attrs={'itemprop': 'description'}).get(
                'content').strip()
        except:
            opisanie = ''
        local_data.append(opisanie)

    # Город (возможно нужно переделать!)
    if city == True:
        try:
            city1 = soup.find('div', attrs={'data-marker': "delivery/map"}).find('span', attrs={
                'data-marker': "delivery/location"}).text.strip()
            city = city1.split(',')[0]
        except:
            city = ''
        local_data.append(city)

    if address == True:
        try:
            adress1 = soup.find('div', attrs={'data-marker': "delivery/map"}).find('span', attrs={
                'data-marker': "delivery/location"}).text.strip().split(',')[1:]
            adress = ','.join(adress1)
        except:
            adress = ''
        local_data.append(adress)

    # Ссылка на объявление
    if ad_link == True:
        pass

    return local_data


def write_txt(data):
    with open('number.txt', 'a') as file:
        file.write('\n' + data)


def write_csv2(data, name):
    with open('/root/www/flask/zakaz/{}'.format(('Заказ №' + name + '.csv')), 'a', encoding='utf-16') as file:
        # order = ['number']
        # fieldnames = data[0]
        writer = csv.writer(file, delimiter=',')
        for line in data:
            writer.writerow(line)

        writer.writerow(line)

def write_xlsx(data, name):
    align_center = Alignment(horizontal='general',
                             vertical='bottom',
                             text_rotation=0,
                             wrap_text=True,
                             shrink_to_fit=False,
                             indent=0)

    # объект
    wb = Workbook()

    # активный лист
    ws = wb.active

    # название страницы
    # ws = wb.create_sheet('первая страница', 0) - создание первой страницы
    ws.title = 'первая страница'

    # align_center = Alignment(wrapText=True)
    for row in data:
        ws.append(row)  # .alignment = Alignment(wrapText=True)

    dims = {}
    for row in ws.rows:
        for cell in row:
            if cell.value:
                dims[cell.column] = max((dims.get(cell.column, 0), len(cell.value)))
    for col, value in dims.items():
        # value * коэфициент
        ws.column_dimensions[col].width = value * 1.5

        wb.save('/root/www/flask/zakaz/{}'.format(('Заказ №' + name + '.xlsx')))


def main(urlss,
         ad_title,
         ad_price,
         contact_face,
         ad_description,
         city,
         address,
         ad_link,
         type_file,):

    app = create_app()
    app.config.from_object(Configuration)
    app.app_context().push()

    job = get_current_job()
    orders = Order.query.filter(Order.id_worker == job.get_id()).all()[-1]
    # send_new_email(orders)  # Выполняет отправку почты
    generate_url = url_generate(urlss)
    urls = []
    urls.append(generate_url)

    iprogress = 0
    urls_page = []  # Список уролов всех страниц
    ob_page = []  # Список объявлений
    shetchik_ob = 0  # Счётчик числа найденных объявлений
    data = data_generate(ad_title=ad_title, ad_price=ad_price, contact_face=contact_face, ad_description=ad_description,
                         city=city, address=address, ad_link=ad_link)

    """
    Проходим по каждому урлу 
    собирая список урлов всех страниц
    """
    for url in urls:

        # Выбираем юзер агент и прокси
        useragents = open('useragents.txt').read().split('\n')
        proxies = open('proxies.txt').read().split('\n')
        local_useragent = {'User-Agent': 'Mozilla/4.0 (compatible; MSIE 5.5; AOL 5.0; Windows 95)'}
        for i in range(1):
            useragent = {'User-Agent': random.choice(useragents)}
            proxy = {'http:': 'http://' + random.choice(proxies)}

        # html страницы с ним работаем
        try:
            html_url = get_html(url, useragent=local_useragent, proxy=proxy)
        except:
            time.sleep(35)
            html_url = get_html(url, useragent=local_useragent, proxy=proxy)

        # Смотрим число объявлений и принимаем решение
        # Если <5.000 или  парсим раздел
        if count_ads(html_url) <= 5000:  # and pagination_on(html_url) == True
            # Ищем число страниц и формируем урды всех страниц раздела
            if '?' in url:
                base_url = url.split("?")[0]
            else:
                base_url = url.strip()
            page_part = '?p='
            if '?' in url:
                qwery_part = url.split('?')[-1]
            else:
                qwery_part = ''

            total_pages = get_total_pages(html_url)
            for i in range(1, total_pages + 1):
                url_gen = base_url + page_part + str(i) + '&' + qwery_part
                urls_page.append(url_gen)

        # Если число объяв больше 5.000,
        # добавляем обратно урл в urls
        elif count_ads(html_url) > 5000:
            ########################################################
            ### Если на странице нету секций (пример компы питер)###
            ########################################################
            if len(find_sections(html_url)) < 1:
                # Ищем число страниц и формируем урды всех страниц раздела
                if '?' in url:
                    base_url = url.split("?")[0]
                else:
                    base_url = url.strip()
                page_part = '?p='
                if '?' in url:
                    qwery_part = url.split('?')[-1]
                else:
                    qwery_part = ''

                total_pages = get_total_pages(html_url)
                for i in range(1, total_pages + 1):
                    url_gen = base_url + page_part + str(i) + '&' + qwery_part
                    urls_page.append(url_gen)
            else:
                for x in find_sections(html_url):
                    urls.append(x)

    """
    Проходим по каждой странице
    и парсим ссылки объявления
    """
    # print(urls_page)
    for ob in urls_page:
        time.sleep(random.randint(1, 3))
        try:
            html = get_html(ob, useragent, proxy)
            # print(html)
        except:
            # print('oshibka')
            time.sleep(60)
            html = get_html(ob)
        # print(html)
        ads = get_page_data(html)  # Список всех объявлений на странице
        for ad in ads:
            url_item = ad.find('a', attrs={'itemprop': 'url'}).get('href')
            bas_url = 'https://m.avito.ru'
            new_urls = bas_url + url_item
            ob_page.append(new_urls)
            shetchik_ob += 1  # Счётчик числа объявлений

            # Собрано объявлений
            job.meta['collected_ob'] = str(len(ob_page))
            job.save_meta()

    """
    Проходим по каждому объявлению 
    и парсим данные
    """

    for mobail_url in ob_page:
        try:
            mobail_html = get_html(mobail_url)
        except:
            time.sleep(60)
            mobail_html = get_html(mobail_url)

        soup = BeautifulSoup(mobail_html, 'lxml')

        data.append(
            item_info(soup, ad_title=ad_title, ad_price=ad_price, contact_face=contact_face,
                      ad_description=ad_description,
                      city=city, address=address, ad_link=ad_link))

        iprogress += 1
        job.meta['save_ads'] = iprogress
        job.meta['progress'] = 100.0 * iprogress / len(ob_page)
        job.save_meta()

    if type_file == 'xlsx':
        write_xlsx(data, str(orders.id))
    else:
        write_csv2(data, str(orders.id))

    if job:
        progress = job.meta['progress']
        if progress >= 100:
            order = Order.query.filter(Order.id_worker == job.get_id()).all()[-1]  # Находим нужную запись
            order.complete = True
            order.number_ads = len(ob_page)
            order.save_ads = iprogress
            db.session.add(order)
            db.session.commit()

            id = str(orders.id)
            email = orders.email
            slug = str(orders.slug)
            # print(type(slug))

            send_new_emails123(id, email, slug)


# 'Connection aborted.', ConnectionResetError(104, 'Connection reset by peer' - ошибка вылетает
# Добавить в html число найденых объявлений
# Сохранение Набре_адс в бд передвинуть повыше


if __name__ == '__main__':
    main()

# 3 in program


"""

"""
