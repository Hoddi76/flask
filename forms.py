from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, IntegerField, SelectField, TextField, \
    TextAreaField
from wtforms.validators import DataRequired, Email, Required, URL, Length


# ckeditor
from flask_ckeditor import CKEditorField

class DemoForm(FlaskForm):
    url_avito = StringField('Ссылка из Авито', validators=[URL(require_tld=True,
                                                               message="Добавьте сначало ссылку на avito")])  # Поле адрес авито, с проверкой на пустое поле
    email = StringField('Email',
                        validators=[DataRequired(message="Добавьте сначало email, иначе мы не сможем прислать данные"),
                                    Email(
                                        message="Адрес электронной почты введен неправильно!")])  # Поле емаил, с проверкой на емаил
    # consent = BooleanField('Я даю соглаcие на обработку персональных данных', validators=[DataRequired(
    # message="Поставьте галочку, что согласны на обработку персональных данных")])  # Галочка согласия на обработку данных
    contact_face = BooleanField('Контактное лицо')
    ad_title = BooleanField('Заголовок объявления')
    ad_description = BooleanField('Описание объявления')
    ad_price = BooleanField('Цена объявления')
    city = BooleanField('Город')
    address = BooleanField('Район, метро, адрес')
    ad_link = BooleanField('Ссылка на объявление')

    type_file = SelectField(u'Тип файла для выгрузки', choices=[('xlsx', 'Файл Excel (xlsx)'), ('csv', 'Файл CSV')])

    download = SubmitField('Скачать данные')  # Кнопка скачать


class FullForm(FlaskForm):
    url_avito = StringField('Ссылка из Авито',
                            validators=[URL(require_tld=True, message="Добавьте сначало ссылку на avito")])
    limit = IntegerField('Лимит объявлений', default=0)

    contact_face = BooleanField('Контактное лицо')
    ad_title = BooleanField('Заголовок объявления')
    ad_description = BooleanField('Описание объявления')
    ad_price = BooleanField('Цена объявления')
    city = BooleanField('Город')
    address = BooleanField('Район, метро, адрес')
    ad_link = BooleanField('Ссылка на объявление')

    type_file = SelectField(u'Тип файла для выгрузки', choices=[('xlsx', 'Файл Excel (xlsx)'), ('csv', 'Файл CSV')])
    pay_btn = SubmitField('Оплатить заказ')

    email = StringField('Email')


class P_Form(FlaskForm):
    """
    В торая форма на странице
    парсера
    """
    email = StringField('Email',
                        validators=[DataRequired(message="Добавьте сначало email, иначе мы не сможем прислать данные"),
                                    Email(message="Адрес электронной почты введен неправильно!")])
    pay_select = SelectField('Способ оплаты', choices=[(1, 'Банковская карта (комиссия 2%)'),
                                                       (2, 'Яндекс.Деньги (комиссия 0,5%)')], default=2)
    pay_btn = SubmitField('Оплатить заказ')


# Сделать проверку для ссылок на авито (url_avito)
# Сделать проверку на число от 0 до 4900

class Feedback(FlaskForm):
    name_user = StringField('Ваше имя', validators=[DataRequired(message="Введите ваше имя")])
    user_email = StringField('Email',
                             validators=[
                                 DataRequired(message="Добавьте email"),
                                 Email(message="Адрес электронной почты введен неправильно!")])
    question = StringField('Ваш вопрос', validators=[DataRequired(message="Добавьте ваш вопрос")])


class Yy_form(FlaskForm):
    pay_btn = SubmitField('Перейти к оплате')


class Post_form(FlaskForm):
    title = StringField('Title')
    body = CKEditorField('Body', validators=[DataRequired()])
    submit = SubmitField()
