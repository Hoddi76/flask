from flask import Flask, redirect, url_for, request, abort, current_app
from flask_mail import Mail
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager
from flask_sqlalchemy import SQLAlchemy
# ckeditor
from flask_ckeditor import *
from flask_wtf.csrf import CSRFProtect

from config import Configuration

from redis import Redis
from rq import Queue

from flask_admin import Admin, AdminIndexView
from flask_admin.contrib.sqla import ModelView

from flask_security import SQLAlchemyUserDatastore, Security, login_user
from flask_security import current_user

from flask_login import LoginManager
from flask_admin import helpers as admin_helpers

db = SQLAlchemy()
migrate = Migrate()
mail = Mail()
admin = Admin()
security = Security()

login_manager = LoginManager()
ckeditor = CKEditor()
csrf = CSRFProtect()

app = Flask(__name__, static_folder='static')
app.config.from_object(Configuration)
with app.app_context():
    app.config.from_object(Configuration)
    db.init_app(app)
    mail.init_app(app)
    admin.init_app(app)
    # Migrate
    migrate.init_app(app, db)
    ckeditor.init_app(app)

manager = Manager(app)
manager.add_command('db', MigrateCommand)


def create_app():
    app = Flask(__name__, static_folder='static')
    app.config.from_object(Configuration)
    with app.app_context():
        app.config.from_object(Configuration)
        db.init_app(app)

        mail.init_app(app)
        admin.init_app(app)
        # Migrate
        migrate.init_app(app, db)
    return app


### ADMIN ###
from models import *


# class BaseModelView(ModelView):
#   """
#  Создание слага из админки
# """
# def on_model_change(self, form, model, is_created):
#   model.generate_slug()
#  return super(BaseModelView, self).on_model_change(form, model, is_created)


class AdminView(ModelView):
    def is_accessible(self):
        return current_user.has_role('adminstration')

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('security.login', next=request.url))


admin.add_view(AdminView(Order, db.session))
admin.add_view(AdminView(User, db.session))
admin.add_view(AdminView(Role, db.session))
admin.add_view(AdminView(Post, db.session))
admin.add_view(AdminView(Categories, db.session))

### Flask-Securiti ###
user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security.init_app(app, datastore=user_datastore)

### Radis ###
q = Queue('avito_syte', connection=Redis(), default_timeout=3600)  # Тайм аут здесь равен часу

# Импорты должны быть сгруппированы в следующем порядке:
# импорты из стандартной библиотеки
# импорты сторонних библиотек
# импорты модулей текущего проекта
