# from app import create_app
# import view
from app import db
from first.blueprint import first
from orders.blueprint import orders
from parser.blueprint import parsers
from blog.blueprint import blog
from app import app
from werkzeug.contrib.fixers import ProxyFix
import os
# app = create_app()
basedir = os.path.abspath(os.path.dirname(__file__))

app.wsgi_app = ProxyFix(app.wsgi_app)
app.register_blueprint(orders, url_prefix='/orders')
app.register_blueprint(first, url_prefix='')
app.register_blueprint(parsers, url_prefix='/parser')
app.register_blueprint(blog, url_prefix='/blog')
app.config['UPLOAD_FOLDER'] = '/root/flask/zakaz'

# ckeditor
app.config['CKEDITOR_SERVE_LOCAL'] = True
app.config['CKEDITOR_HEIGHT'] = 400
app.config['CKEDITOR_FILE_UPLOADER'] = 'upload'
app.config['UPLOADED_PATH'] = os.path.join(basedir, 'uploads')

if __name__ == '__main__':
    app.run(debug=True, threaded=True)
