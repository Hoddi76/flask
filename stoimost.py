from bs4 import BeautifulSoup
import requests
import time


def get_html(url):
    r = requests.get(url)
    return r.text


def x(html):
    soup = BeautifulSoup(html, "lxml")
    number = soup.find('div', class_='l-content').find('span', class_='breadcrumbs-link-count').text.strip()
    number1 = number.split(' ')
    number2 = ''.join(number1)
    return number2


def order_stoimost(url):
    # breadcrumbs-link-count
    s = get_html(url)
    a = x(s)
    full_ob = int(a)
    summa = int(full_ob) * 0.03
    if summa < 2:
        summa = 2
    return summa, str(full_ob)

# if __name__ == '__main__':
# stoimost_zakaza()
