from flask import Blueprint, render_template, redirect, url_for, send_from_directory
from models import Post, Categories

from flask import request
from forms import Post_form
from app import db, app
from flask_security import login_required

from flask_ckeditor import upload_success, upload_fail
import os
# upload image prevu
from werkzeug.utils import secure_filename


def allowed_file(filename):
    ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


blog = Blueprint('blog',
                 __name__,
                 template_folder='templates',
                 static_folder='static', )


@blog.route('/files/<filename>')
def uploaded_files(filename):
    return send_from_directory(app.config['UPLOADED_PATH'], filename)


@app.route('/upload', methods=['POST'])
def upload():
    f = request.files.get('upload')
    extension = f.filename.split('.')[1].lower()
    if extension not in ['jpg', 'gif', 'png', 'jpeg']:
        return upload_fail(message='Image only!')
    f.save(os.path.join(app.config['UPLOADED_PATH'], f.filename))
    url = url_for('blog.uploaded_files', filename=f.filename)
    return upload_success(url=url)


@blog.route('/uploads/<filename>')
def send_file(filename):
    return send_from_directory(app.config['UPLOADED_PATH'], filename)


@blog.route('/create', methods=['POST', 'GET'])
@login_required
def create_post():
    form = Post_form()
    if form.validate_on_submit():
        file = request.files['file']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOADED_PATH'], filename))
        new_post = Post(title=form.title.data, body=form.body.data, link_image=filename)
        db.session.add(new_post)
        db.session.commit()
        return redirect(url_for('blog.index'))
    return render_template('/create_post.html', form=form)


@blog.route('/<slug>/edit/', methods=['GET', 'POST'])
@login_required
def edit_post(slug):
    post = Post.query.filter(Post.slug == slug).first_or_404()

    if request.method == 'POST':
        form = Post_form(fromdata=request.form, obj=post)
        form.populate_obj(post)
        db.session.commit()

        return redirect(url_for('blog.post_detail', slug=post.slug))

    form = Post_form(obj=post)
    return render_template('/edit_post.html', post=post, form=form)


@blog.route('/')
def index():
    q = request.args.get('q')
    page = request.args.get('page')
    if page and page.isdigit():
        page = int(page)
    else:
        page = 1

    # Поиск
    if q:
        posts = Post.query.filter(Post.title.contains(q) | Post.body.contains(q))
    else:
        posts = Post.query.order_by(Post.id.desc())

    # Если мы пользуемся поиском posts будет списком постов
    pages = posts.paginate(page=page, per_page=3)
    return render_template('blog.html', posts=posts, pages=pages)


@blog.route('/<slug>')
def post_detail(slug):
    post = Post.query.filter(Post.slug == slug).first_or_404()
    return render_template('post_detail.html', post=post)


# Делаем вьюху категории
# Не используется не в одном шаблоне
# Шаблон ещё даже не создан (можно переделать главный шаблон)
@blog.route('/categories/<slug>')
def categories_detail(slug):
    categorie = Categories.query.filter(Categories.slug == slug).first_or_404()
    posts = categorie.posts.all()
    return render_template('tag_detail.html', categorie=categorie, posts=posts)
