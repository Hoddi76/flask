from flask import Blueprint, render_template, flash, request, send_from_directory

first = Blueprint('', __name__, template_folder='templates', static_folder='static')

from forms import Feedback
from flask import request

from app import app


@first.route('/')
def index():
    return render_template('first/index.html')


@first.route('/robots.txt')
def static_from_root():
    return send_from_directory('/root/flask/static', 'robots.txt')


def feedback():
    """
    Форма обратной связи
    """
    form = Feedback()
    if form.validate_on_submit():
        if request.method == 'POST':
            name_user = request.form['name_user']
            # print(name_user)


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404
