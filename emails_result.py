from flask_mail import Message
from app import mail, app
from flask import render_template, current_app

#app = create_app()


def send_email(subject, sender, recipients, text_body, html_body):
    msg = Message(subject, sender=sender, recipients=recipients)
    msg.body = text_body
    msg.html = html_body
    with app.app_context():
        mail.send(msg)


def send_new_emails(orders):
    send_email('Заказ № ' + (str(orders.id)) + ' на Avito-Parser',  # Незабыть исправить заголовок
               sender='sapport@avito-parser24.ru',
               recipients=[orders.email],
               text_body=render_template('emails_result.txt',
                                         orders=orders),
               html_body=render_template('emails_result.html',
                                         orders=orders))


def send_new_emails123(id, email, slug):
    send_email('Заказ № ' + id + ' на Avito-Parser24.ru',  # Незабыть исправить заголовок
               sender='sapport@avito-parser24.ru',
               recipients=[email],
               text_body=render_template('emails_result.txt',
                                         slug=slug, id=id),
               html_body=render_template('emails_result.html',
                                         slug=slug, id=id))
